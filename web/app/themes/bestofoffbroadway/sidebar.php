<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package WordPress
 * @subpackage BestofOffBroadway
 * @since BestofOffBroadway_1.0
 */
?>

	<?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
		<div id="secondary" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
			<div id="social"><span class="follow">FOLLOW:</span><a href="http://www.facebook.com/BestofOffBroadway"><img src="<?php echo bloginfo("template_directory");?>/images/facebook.png"></a><a href="http://twitter.com/bestofoffbway"><img src="<?php echo bloginfo("template_directory");?>/images/twitter.png"></a>
				<div class="g-plusone" data-size="standard" data-annotation="none"></div>
				
				<script type="text/javascript">
				  (function() {
				    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				    po.src = 'https://apis.google.com/js/plusone.js';
				    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				  })();
				</script>
				</div>
				
		</div><!-- #secondary -->
	<?php endif; ?>