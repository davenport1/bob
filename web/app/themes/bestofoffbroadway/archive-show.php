<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BestofOffBroadway
 * @since BestofOffBroadway_1.0
 */

get_header(); ?>

	<section id="primary" class="site-content show-list">
		<div id="content" role="main">
			<div id="text-bg">
			<?php
			/* Start the Loop */
			echo '<div class="cat-title">OFF-BROADWAY MUSICALS</div>';
			$musicals= get_posts(array(
			'post_type' => 'show',
			'meta_key' => 'type',
			'meta_value' => 'Musical',
			'orderby'=> 'title',
			'order'=> 'ASC',	
			'numberposts' => -1
			));
			foreach ($musicals as $show):
				$closedate=get_field('closing_date', $show->ID);
				if(!$closedate||$closedate>date("Ymd")){
					echo '<div class="list-item"><a href="' . get_permalink($show->ID) . '"><img class="list-image" src="' . get_field('show_feature_logo', $show->ID) . '"></a>
					<div class="list-text"><a class="list-title" href="' . get_permalink($show->ID) . '">' . get_the_title($show->ID) .'</a>';
						
						$position=200;
						$message=get_field('synopsis',$show->ID);								
						
						if (strlen($message)>200){
							$post = substr($message,$position,1);
				
							if($post !=" "&&$post!=","){

								while($post !=" "&&$post!=","){
									$i=1;
									$position=$position+$i;
									$post = substr($message,$position,1); 
								}
							}
				
							$post = substr($message,0,$position); 
						}
						
						else {
							$post=$message;
						}
				
						echo'<div class="featured-synopsis">' . $post . '...</div>';
					
					echo'</div></div>';
					
			
				}
					
			endforeach;	
			
			/* Start the Loop */
			echo '<div class="cat-title">OFF-BROADWAY PLAYS</div>';
			$plays= get_posts(array(
			'post_type' => 'show',
			'meta_key' => 'type',
			'meta_value' => 'Play',
			'orderby'=> 'title',
			'order'=> 'ASC',	
			'numberposts' => -1
			));
			foreach ($plays as $show):
				$closedate=get_field('closing_date', $show->ID);
				if(!$closedate||$closedate>date("Ymd")){
					echo '<div class="list-item"><a href="' . get_permalink($show->ID) . '"><img class="list-image" src="' . get_field('show_feature_logo', $show->ID) . '"></a>
					<div class="list-text"><a class="list-title" href="' . get_permalink($show->ID) . '">' . get_the_title($show->ID) .'</a>';
					
						$position=250;
						$message=get_field('synopsis',$show->ID);
							$position=250;
							$message=get_field('synopsis',$show->ID);								
							
							if (strlen($message)>250){
								$post = substr($message,$position,1);
					
								if($post !=" "&&$post!=","){

									while($post !=" "&&$post!=","){
										$i=1;
										$position=$position+$i;
										$post = substr($message,$position,1); 
									}
								}
					
								$post = substr($message,0,$position); 
							}
							
							else {
								$post=$message;
							}
					
							echo'<div class="featured-synopsis">' . $post . '...
							</div>';
					
					echo'</div>';
					
					echo '</div>';
				}
					
			endforeach;			?>
			
			<?php
			/* Start the Loop */
			echo '<div class="cat-title">SPECIAL EVENTS</div>';
			$events= get_posts(array(
			'post_type' => 'show',
			'meta_key' => 'type',
			'meta_value' => 'Special Events',
			'orderby'=> 'title',
			'order'=> 'ASC',	
			'numberposts' => -1
			));
			foreach ($events as $show):
				$closedate=get_field('closing_date', $show->ID);
				if(!$closedate||$closedate>date("Ymd")){
					echo '<div class="list-item"><a href="' . get_permalink($show->ID) . '"><img class="list-image" src="' . get_field('show_feature_logo', $show->ID) . '"></a>
					<div class="list-text"><a class="list-title" href="' . get_permalink($show->ID) . '">' . get_the_title($show->ID) .'</a>';
					
						$position=250;
						$message=get_field('synopsis',$show->ID);
							$position=250;
							$message=get_field('synopsis',$show->ID);								
							
							if (strlen($message)>250){
								$post = substr($message,$position,1);
					
								if($post !=" "&&$post!=","){

									while($post !=" "&&$post!=","){
										$i=1;
										$position=$position+$i;
										$post = substr($message,$position,1); 
									}
								}
					
								$post = substr($message,0,$position); 
							}
							
							else {
								$post=$message;
							}
					
							echo'<div class="featured-synopsis">' . $post . '...
							</div>';
					
					echo'</div>';
					
					echo '</div>';
				}
					
			endforeach;			?>
			
			<div class="cat-title closed"><a href="<?php echo site_url() ?>/closed">CLOSED SHOWS</a></div>

			
			</div>		


		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>