<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage BestofOffBroadway
 * @since BestofOffBroadway_1.0
 */

get_header(); ?>

	<div id="primary" class="site-content theater">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
			<?php
			
			echo '<div id="show-text-bg">
			<div id="show-text">
			<div class="top">
			<h1 class="title"> '.get_the_title().'</h1>';
			echo'<h2>Theater</h2><div class="theater">' . get_the_title() . '</div>';
		$location = get_field('location');
		$location_description = str_replace(', United States', '', $location['address']);
		$location_description = str_replace(', USA', '', $location_description);
		echo '<div class="address">'.str_replace(', United States', '', $location_description).'</div>';					
			echo '</div>';
			echo '<img class="map" src="http://maps.googleapis.com/maps/api/staticmap?center='.$location['address'].'&zoom=16&size=300x200&markers=color:red|'.$location['address'].'&sensor=false">';			
				echo '<div id="about">ABOUT THE THEATER</div>';
				
				
				echo '<div class="theater-info">';
				$neighborhoods=get_field("neighborhood");
			
				
				if ($neighborhoods){
					echo'<div class="theater-block"><span class="theater-head">Neighborhood:</span>';
						echo '<span class="theater-text">'.get_the_title($neighborhoods[0]).'</span></div>';
				}	
				
				if (get_field("specific_location")){
					echo'<div class="theater-block"><span class="theater-head">Location:</span>';
						echo '<span class="theater-text">'.get_field("specific_location").'</span></div>';
				}
				
				if (get_field("directions")){
					echo'<div class="theater-block"><span class="theater-head">Directions:</span>';
						echo '<span class="theater-text">'.get_field("directions").'</span>';
						if (get_field("hopstop_url")){
								echo '</br><a class="theater-text" href="'.get_field("hopstop_url").'">Click here for Hopstop Directions</a>';
						}
						echo'</div>';
				}
				
				if (get_field("stage_type")){
					echo'<div class="theater-block"><span class="theater-head">Stage Type:</span>';
						echo '<span class="theater-text">'.get_field("stage_type").'</span></div>';
				}
				
				$accessibility=get_field("accessibility");
				
				if ($accessibility){
					echo'<div class="theater-block"><span class="theater-head">Accessibility:</span>';
						echo '<span class="theater-text">';
						for($i=0;$i<count($accessibility);$i++){
							echo $accessibility[$i];
							if($i<count($accessibility)-1){
								echo ', ';
							}
						}
						echo '</span></div>';
				}
				
				if (get_field("seating_chart")){
					echo'<div class="theater-block"><span class="theater-head">Seating Chart:</span>';
						echo '<a class="theater-text" href="'.get_field("seating_chart").'">Click here for seating</a></div>';
				}
				
				if (get_field("theater_website")){
					echo'<div class="theater-block"><span class="theater-head">Theater Website:</span>';
						echo '<a class="theater-text" href="'.get_field("theater_website").'">'.get_field("theater_website").'</a></div>';
				}
				
				echo '</div>';
				
				echo '<div id="current-show">';
				echo '<div class="current-title">CURRENT PRODUCTION:</div>';
				
				$current= get_posts(array(
								'post_type' => 'show',
								'meta_query' => array(
									array(
										'key' => 'theater', // name of custom field
										'value' => '"' . get_the_ID() . '"',
										'compare' => 'LIKE'
									)
								)
							));
							
							$count=true;
						for($i=0;$i<count($current);$i++){
							$closedate=get_field('closing_date', $current[$i]->ID);
							if(!$closedate||$closedate>date("Ymd")){
								$photo = get_field('show_logo', $current[$i]->ID);
								echo '<a href="' . get_permalink($current[$i]->ID) .'"><img src="'.$photo.'"></a>';
								echo '<div class="button"><a class="buy" href="'.get_field("buy_tickets_link",$current[$i]->ID).'">BUY TICKETS</a></div>';
								$count=false;
								break;
							}
						}
						
						if($count==true){
							echo '<img class="dark" src="';
							echo bloginfo("template_directory");
							echo'/images/dark.png">';
							
						}
						
				
				echo '</div>';
				
				echo '<div id="about">NEARBY</div>';
		
				$neighborhood= get_field('neighborhood');
				
				echo '<div id="nearby-1">';
				
				$parking_rows=get_field('parking',$neighborhood[0]->ID);
				if($parking_rows)
				{
					echo '<h2>Parking:</h2>';

					/*foreach($parking_rows as $row)
					{
						echo '<div class="nearby-block"><div class="name">' . $row['name'] . '</div><div class="location">' . $row['location'] . '</div><div class="phone-number">' . $row['phone_number'] . '</div><a class="url" href="' . $row['url'] . '">' . $row['url']. '</a></div>';
					}*/
				}
				
				$hotel_rows=get_field('hotel',$neighborhood[0]->ID);
				if($hotel_rows)
				{
					echo '<h2>Hotels:</h2>';

					/*foreach($hotel_rows as $row)
					{
						echo '<div class="nearby-block"><div class="name">' . $row['name'] . '</div><div class="location">' . $row['location'] . '</div><div class="phone-number">' . $row['phone_number'] . '</div><a class="url" href="' . $row['url'] . '">' . $row['url']. '</a></div>';
					}*/
				}
				
				echo '</div><div id="nearby-2">';
				
				$restaurant_rows=get_field('restaurant',$neighborhood[0]->ID);
				if($restaurant_rows)
				{
					echo'<h2>Restaurants:</h2>';

					/*foreach($restaurant_rows as $row)
					{
						echo '<div class="nearby-block"><div class="name">' . $row['name'] . '</div><div class="type">' . $row['restaurant_type'] . '</div><div class="location">' . $row['location'] . '</div><div class="phone-number">' . $row['phone_number'] . '</div><a class="url" href="' . $row['url'] . '">' . $row['url']. '</a></div>';
					}*/
				}
				
				echo '</div>';
				
				echo '<div id="about">PAST PRODUCTIONS</div>';
				echo '<div id="closed">';
				
				for($i=0;$i<count($current);$i++){
					$closedate=get_field('closing_date', $current[$i]->ID);
					if($closedate&&$closedate<date("Ymd")){
						$photo = get_field('show_logo', $current[$i]->ID);
						echo '<a href="' . get_permalink($current[$i]->ID) .'"><img class="closed" src="'.$photo.'"></a>';
					}
				}
				
				echo '</div>';
			
			 comments_template( '', true ); 
			
			echo '</div></div>';
			?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>