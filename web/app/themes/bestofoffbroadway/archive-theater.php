<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage BestofOffBroadway
 * @since BestofOffBroadway_1.0
 */

get_header(); ?>

	<section id="primary" class="site-content theater-list">
		<div id="content" role="main">
			<div id="text-bg">
				<table id='theater-list'>
					<thead>
						<tr>
							<th>THEATER</th>
							<th>NEIGHBORHOOD</th>
							<th>CURRENT SHOW</th>
						</tr>
					</thead>
					<tbody>

			<?php
			/* Start the Loop */
			$theaters= get_posts(array(
			'post_type' => 'theater',
			'orderby'=> 'title',
			'order'=> 'ASC',	
			'numberposts' => -1
			));
			foreach ($theaters as $theater):	
			
			echo '<tr><td><a class="theater-title" href="' . get_permalink($theater->ID) . '">' . get_the_title($theater->ID) . '</a></td>';
			$neighborhoods=get_field("neighborhood",$theater->ID);
			if ($neighborhoods) echo  '<td>' . get_the_title($neighborhoods[0]) . '</td>';
			else echo "<td> </td>";
			
					$current= get_posts(array(
									'post_type' => 'show',
									'meta_query' => array(
										array(
											'key' => 'theater', // name of custom field
											'value' => '"' . $theater->ID . '"',
											'compare' => 'LIKE'
										)
									)
								));

								$count=true;
							for($i=0;$i<count($current);$i++){
								$closedate=get_field('closing_date', $current[$i]->ID);
								if(!$closedate||$closedate>date("Ymd")){
									echo '<td> <a href="' . get_permalink($current[$i]->ID) .'">' . get_the_title($current[$i]->ID) . '</a></td>';
									$count=false;
									break;
								}
							}

							if($count==true){
								echo '<td> Show Coming Soon </td>';

							}

							echo '</tr>';
							
							endforeach;	
			
			?>
			</tbody>
			</table>
		</div>
		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>