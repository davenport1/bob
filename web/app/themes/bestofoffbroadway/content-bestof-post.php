<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage BestofOffBroadway
 * @since BestofOffBroadway_1.0
 */
?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header">
			<h1 class="entry-title">
				<?php the_title(); ?>
			</h1>
		</header><!-- .entry-header -->

		<div class="entry-summary">
			<?php the_content(); ?>
		</div><!-- .entry-summary -->

		<footer class="entry-meta">
			<?php bestofoffbroadway_entry_meta(); ?>
			<?php edit_post_link( __( 'Edit', 'bestofoffbroadway' ), '<span class="edit-link">', '</span>' ); ?>
		</footer><!-- .entry-meta -->
	</article><!-- #post -->
