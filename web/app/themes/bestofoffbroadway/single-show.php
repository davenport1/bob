<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage BestofOffBroadway
 * @since BestofOffBroadway_1.0
 */

get_header(); ?>

	<div id="primary" class="site-content show">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
			<?php
			if (get_field("show_full_image") == "") {
			$full_image = 'http://www.bestofoffbroadway.com/wp-content/uploads/2014/04/bike3.jpg';			
			}
			else {
			$full_image = get_field("show_full_image");
			}
			echo '<div id="show-callout">
			<img src="'.$full_image.'" class="full">
			<img src="'.get_field("show_logo").'" class="logo">
			<div id="text-bg"><div id="text">			
			<h1 class="title"> '.get_the_title().'</h1>';
			
			$high_price=get_field("high_price");
			
			echo '<div class="price"> Tickets from $'.get_field("low_price");
			echo '</div></div>';
			
			$closedate=get_field('closing_date', $show->ID);
			
			if(!$closedate||$closedate>date("Ymd")){
				echo'<div class="buttons">
				<div class="button"><a href="'.get_field("buy_tickets_link").'">BUY TICKETS</a></div>
				<div class="button"><a href="'.get_field("check_discounts_link").'">CHECK DISCOUNTS</a></div>
				</div>';
			}
			
			echo'</div></div>';
			
			echo '<div id="show-text-bg">
			<div id="show-text">
			<div class="top">
			<h1 class="title"> '.get_the_title().'</h1>';
				
			$theaters=get_field("theater");
			foreach($theaters as $theater):
				echo '
					<h2>Theater</h2><a class="theater" href='.get_permalink($theater->ID).'>'.get_the_title($theater->ID).'</a>';
				$location = get_field('location',$theater->ID);
				$location_description = str_replace(', United States', '', $location['address']);
				$location_description = str_replace(', USA', '', $location_description);
				
				
				
				echo '<div class="address">'.$location_description.'</div>';
			endforeach;
				
				echo '<h2 class="tickets">Tickets</h2>';
				if($high_price)	echo '<div id="main-price"> $'.get_field("low_price").' - $'.$high_price;
				else echo '<div id="main-price"> Tickets from $'.get_field("low_price");
								
				echo '</div>';
				if(!$closedate||$closedate>date("Ymd")){
				echo'<div id="buttons"><div class="button"><a class="buy" href="'.get_field("buy_tickets_link").'">BUY TICKETS</a></div><div class="button"><a class="discounts" href="'.get_field("check_discounts_link").'">CHECK DISCOUNTS</a></div></div>';
				}
				
				echo'</div>';
				
			foreach($theaters as $theater):
				echo '<img class="map" src="http://maps.googleapis.com/maps/api/staticmap?center='.$location['address'].'&zoom=16&size=300x200&markers=color:red|'.$location['address'].'&sensor=false">';
			endforeach;
				
				echo '<div id="about">ABOUT THE SHOW</div>';
				
				if (get_field("synopsis")){
					echo'<h2>SYNOPSIS:</h2>';
					echo '<div class="synopsis">'.get_field("synopsis").'</div>';
				}	
				
				$open=get_field("opening_date");
				echo '<h2>PERFORMANCE DATES:</h2>
				<div class="date">Opening: '.date('F j, Y', strtotime($open)).'</br>
				Closing: ';
				if (get_field("closing_date")) echo date('F j, Y', strtotime(get_field("closing_date")));
				else echo 'Open ended run';
				echo '</div>';
				
				if (get_field("running_time")){
					echo '<h2>Running Time:</h2>';
					echo '<div class="running-time">'.get_field("running_time").'</div>';
				}
				
				if (get_field("did_he_like_it_review")) {
					echo '<h2>DID HE LIKE IT?</h2>';
					echo '<img src="';
					echo bloginfo("template_directory");
					echo '/images/';
					if(get_field("did_he_like_it_review")=="Thumbs Up") echo 'did_he_up.png';
					else if(get_field("did_he_like_it_review")=="Thumbs Down") echo 'did_he_down.png';
					else if(get_field("did_he_like_it_review")=="On The Fence") echo 'did_he_fence.png';
					echo '" class="did-he" title="" alt="" />';
					echo '<div class="did-he"><div class="synopsis">'.get_field("did_he_like_it_synopsis").'</div>
					<a href="'.get_field("did_he_like_it_link").'">READ COMPLETE REVIEWS AT DIDHELIKEIT.COM</a></div>';
			}
			
			if (get_field("website")){
				echo '<h2>WEBSITE:</h2>';
				echo '<a href="'.get_field("website").'" class="website">'.get_field("website").'</a>';
			}
			
			echo '</div></div>';
			
			 comments_template( '', true ); 
			 ?>

			<?php endwhile; // end of the loop. ?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>