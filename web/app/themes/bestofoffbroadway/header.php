<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage BestofOffBroadway
 * @since BestofOffBroadway_1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<!--<link rel="stylesheet" href="<?php //echo bloginfo("template_directory");?>/css/flexslider.css" type="text/css">-->
<link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
<!--<script src="<?php echo bloginfo("template_directory");?>/js/jquery.flexslider.js"></script>-->
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->

<!--
<script type="text/javascript" charset="utf-8">
  $(window).load(function() {
    $('.flexslider').flexslider();
  });
</script>
-->

<script type="text/javascript">
(function() {
  var form = document.getElementById("mad_mimi_signup_form"),
      submit = document.getElementById("webform_submit_button"),
      email = /.+@.+\..+/;

  form.onsubmit = function(event) {
    var isValid = validate();
    if(!isValid) {
      for(var i = 0; i < form.elements.length; ++i) {
        var input = form.elements[i];
        if(input.className.indexOf("required") >= 0) {
          input.onchange = validate;
        }
      }
      return false;
    }
    if(form.getAttribute("target") != "_blank") {
      form.className = "mimi_submitting";
      submit.value = submit.getAttribute("data-submitting-text");    
  		submit.disabled = true;
  		submit.className = "disabled";
    }

    setTimeout(function() {
      for(var i = 0; i < form.getElementsByTagName("input").length; ++i) {
        var input = form.getElementsByTagName("input")[i];
        if(input.getAttribute("type") == "text") {
          input.value = "";
        }
        if(input.id == "signup_email") {
          input.placeholder = "you@example.com";
        } else {
          input.placeholder = "";
        }
      }
    }, 3000);
  };

  function validate() {
    var isValid = true;

    for(var i = 0; i < form.elements.length; ++i) {
      var input = form.elements[i],
          allDivs = input.parentNode.getElementsByTagName("div");

      if(input.className.indexOf("required") >= 0) {
        if(input.id == "signup_email") {
          if(!email.test(input.value)) {
            emailErrorMessage(input, allDivs);
            isValid = false;
          } else {
            removeErrorMessage(input, allDivs);
          }
        } else {
          if((input.type == "checkbox" && !input.checked) || input.value == "" || input.value == "-1") {
            fieldErrorMessage(input, allDivs);
            isValid = false;
          } else {
            removeErrorMessage(input, allDivs);
          }
        }
      }
    }

    form.className = isValid ? "" : "mimi_invalid";
    submit.value = isValid ? submit.getAttribute("data-default-text") : submit.getAttribute("data-invalid-text");
		submit.disabled = !isValid;
		submit.className = isValid ? "submit" : "disabled";

    return isValid;
  }

  function emailErrorMessage(input, allDivs) {
    if(input.getAttribute("data-webform-type") == "iframe") {
      input.className = "required invalid";
      input.placeholder = input.getAttribute("data-required-message") || "This field is required";
    } else {
      allDivs[0].innerHTML = input.getAttribute("data-invalid-message") || "This field is invalid";
    }
  }

  function fieldErrorMessage(input, allDivs) {
    if(input.getAttribute("data-webform-type") == "iframe") {
      input.className = "required invalid";
      input.placeholder = input.getAttribute("data-required-message") || "This field is required";
    } else {
      for(var i = 0; i < allDivs.length; ++i) {
        var element = allDivs[i];
        if(element.className.indexOf("mimi_field_feedback") >= 0) {
          return element.innerHTML = input.getAttribute("data-required-message") || "This field is required";
        }
      }
    }
  }

  function removeErrorMessage(input, allDivs) {
    if(input.getAttribute("data-webform-type") == "iframe") {
      input.className = "required";
    }

    for(var i = 0; i < allDivs.length; ++i) {
      var element = allDivs[i];
      if(element.className.indexOf("mimi_field_feedback") >= 0) {
        return element.innerHTML = "";
      }
    }
  }
})();
</script>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<header id="masthead" class="site-header" role="banner">
		<a href="<?php echo get_option('home'); ?>"><img class="logo" src="<?php echo bloginfo("template_directory");?>/images/logo.png"> </a></br>
		<?php dynamic_sidebar( 'top-1' ); ?>
		<div class="banner"><div class="header-text">GET THE COMPLETE LIST OF DISCOUNT OFFERS TO OFF-BROADWAY SHOWS! </div>
		
	<div class="subscribe-wrapper">
		<!--<input id="ml_signup_email" name="email" type="text" value="" />
   <input name="ml_groups[]" value="495079" type="hidden" />
		<button id="ml_subscribe">Subscribe</button>-->
		<form action="//theproducersperspective.us5.list-manage.com/subscribe/post?u=535faf11c70ac0e262b4138e7&amp;id=8175dcb7b3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">

	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;"><input type="text" name="b_535faf11c70ac0e262b4138e7_8175dcb7b3" tabindex="-1" value=""></div>
    <div class="mc-field-group" style="display:inline;">
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL" >
	<input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
</div>
    </div>
</form>
		
	</div>
         		
		</div>
		
		<?php 
		
		if(get_the_title()=='Home'){
		//echo '<div>';
		echo do_shortcode("[metaslider id=5816]");
		//echo '</div>';
		}
		
		?>
		
		<?php 
		/*
		if(get_the_title()=='Home'){
			echo '<a href="http://www.bestofoffbroadway.com/show/the-awesome-80s-prom/"><img src="http://www.bestofoffbroadway.com/wp-content/uploads/2013/10/awesome80sprom.jpg" /></a>';				
		}
		*/
		?>
		
		<?php 
		/*
		if(get_the_title()=='Home'){
			echo'<div class="flexslider">
			  <ul class="slides">';
					$ID = get_page_ID('carousel-images');
					$carousel_rows=get_field('image',$ID);
					if($carousel_rows)
					
					/*added code for testing
					
					
					
					echo '0 = ' . $carousel_rows[0];
					echo ' | 1 = ' . $carousel_rows[1];
					echo ' | 2 = ' . $carousel_rows[2];
					echo ' | 3 = ' . $carousel_rows[3];
					echo ' | 4 = ' . $carousel_rows[4];
					echo ' | 5 = ' . $carousel_rows[4];
					echo ' | 6 = ' . $carousel_rows[4];
					
					
					
					
					
					
					
					
					{
					
						//echo '<li><a href="http://www.bestofoffbroadway.com/show/the-awesome-80s-prom/"><img src="http://www.bestofoffbroadway.com/wp-content/uploads/2013/10/awesome80sprom.jpg" /></a></li>';
						
						echo '<li><a href="http://www.bestofoffbroadway.com/show/the-awesome-80s-prom/"><img src="http://www.bestofoffbroadway.com/wp-content/uploads/2013/10/awesome80sprom.jpg" /></a></li>';
						//echo '<li><a href="http://www.bestofoffbroadway.com/show/the-awesome-80s-prom/"><img src="http://www.bestofoffbroadway.com/wp-content/uploads/2013/10/awesome80sprom.jpg" /></a></li>';
						///echo '<li><a href="http://www.bestofoffbroadway.com/show/the-awesome-80s-prom/"><img src="http://www.bestofoffbroadway.com/wp-content/uploads/2013/10/avenueq.jpg" /></a></li>';
						
						
						
						foreach($carousel_rows as $row)
						{
						
						//echo 'url = ' . $row['url'];
						//echo ' | c image = ' . $row['carousel_image'];	
							/*echo '<li>
							      <a href="' . $row['url'] . '"><img src="' . $row['carousel_image'] . '" />';
							    echo '</a></li>';
							
						}
					
					
					}
			
			echo'</ul>
			</div>';
		}
		*/
		?>
		
		
		
		<nav id="site-navigation" class="main-navigation <?php if(get_the_title()=='Home') echo 'home-nav'?>" role="navigation">
			<h3 class="menu-toggle"><?php _e( 'Menu', 'bestofoffbroadway' ); ?></h3>
			<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'bestofoffbroadway' ); ?>"><?php _e( 'Skip to content', 'bestofoffbroadway' ); ?></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
		</nav><!-- #site-navigation -->

<!-- FB Remarketing Script-->


<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
_fbq.push(['addPixelId', '817435971603750']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=817435971603750&amp;ev=PixelInitialized" /></noscript>


<!-- End Script -->


	</header><!-- #masthead -->

	<div id="main" class="wrapper">