<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage BestofOffBroadway
 * @since BestofOffBroadway_1.0
 */

get_header(); ?>

	<div id="primary" class="site-content home">
		<div id="content" role="main">


            <!-- The Loop -->
			<?php while ( have_posts() ) : the_post(); 
				echo'<div id="text-bg">';
					echo '<div class="home-title">THE "BEST" OF OFF-BROADWAY</div>';
					
					$shows=get_field("shows");
					foreach($shows as $show):		
				 	echo '<div class="featured-show">
							<a href="' . get_permalink($show->ID) . '"><img class="featured-image" src="' . get_field('show_feature_logo',$show->ID) . '">
							<div class="featured-text">
								<div class="featured-title">'. get_the_title($show->ID) . '</div></a>';
						
								$position=250;
								$message=get_field('synopsis',$show->ID);								
								
								if (strlen($message)>250){
									$post = substr($message,$position,1);
						
									if($post !=" "&&$post!=","){

										while($post !=" "&&$post!=","){
											$i=1;
											$position=$position+$i;
											$post = substr($message,$position,1); 
										}
									}
						
									$post = substr($message,0,$position); 
								}
								
								else {
									$post=$message;
								}
						
								echo'<div class="featured-synopsis">' . $post . '...
								</div>
								<div class="featured-links">
								<ul>';
								echo '<li><a href="' . get_field('buy_tickets_link',$show->ID) . '">BUY TICKETS</a></li>';
								
								if(get_field('did_he_like_it_link',$show->ID)){
									echo '<li><a href="' . get_field('did_he_like_it_link',$show->ID) . '">REVIEWS</a></li>';
								}
								
								echo '<li><a href="' . get_permalink($show->ID) . '">MORE INFO</a></li>
								</ul>
								</div>
					 	</div>
					</div>'; 
					endforeach;
					echo '<a href="show"><img class="front-callout" src="';
					echo bloginfo("template_directory");
					echo '/images/front-callout.png"></a>';
				echo '</div>';
				?>
				
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>