<?php
/*
Plugin Name: Best of Off-Broadway
Plugin URI: http://bestofoffbroadway.com
Description: Declares a plugin to create custom Show and Theater types
Version: 1.0
Author: Scott Peterman
License: GPLv2
*/
 
add_action( 'init', 'create_post_type' );
function create_post_type() {
	register_post_type( 'show',
		array(
			'labels' => array(
				'name' => __( 'Shows' ),
				'singular_name' => __( 'Show' ),
				'add_new' => _x('Add New', 'Show'), 
				'add_new_item' => __('Add New Show'), 
				'edit_item' => __('Edit Show'), 
				'new_item' => __('New Show'), 
				'view' => __('View Show'),
				'view_item' => __('View Show'), 
				'search_items' => __('Search Show'),
			),
		'public' => true,
		'has_archive' => true,
		'supports'=>array('title'),
		)
	);
	
	register_post_type( 'theater',
		array(
			'labels' => array(
				'name' => __( 'Theaters' ),
				'singular_name' => __( 'Theater' ),
				'add_new' => _x('Add New', 'Theater'), 
				'add_new_item' => __('Add New Theater'), 
				'edit_item' => __('Edit Theater'), 
				'new_item' => __('New Theater'), 
				'view' => __('View Theater'),
				'view_item' => __('View Theater'), 
				'search_items' => __('Search Theater'),
			),
		'public' => true,
		'has_archive' => true,
		'supports'=>array('title'),
		)
	);
	register_post_type( 'neighborhood',
		array(
			'labels' => array(
				'name' => __( 'Neighborhoods' ),
				'singular_name' => __( 'Neighborhood' ),
				'add_new' => _x('Add New', 'Neighborhood'), 
				'add_new_item' => __('Add New Neighborhood'), 
				'edit_item' => __('Edit Neighborhood'), 
				'new_item' => __('New Neighborhood'), 
				'view' => __('View Neighborhood'),
				'view_item' => __('View Neighborhood'), 
				'search_items' => __('Search Neighborhoods'),
			),
		'public' => true,
		'has_archive' => true,
		'supports'=>array('title'),
		)
	);
}
?>
